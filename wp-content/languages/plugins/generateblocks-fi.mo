��    &      L  5   |      P  W   Q     �     �     �     �     �     �  	   �       1      ,   R          �  	   �     �  	   �  )   �     �     �     �                 
   2  
   =  �   H     �  -   �          !     1     =  	   I     S     [     a     |  '  �  S   �          /     8  
   ?     J     e     �     �  Q   �  5   �     0	     9	     @	     O	  	   V	  >   `	     �	     �	     �	     �	     �	     �	     	
     
  �   (
     �
  -   �
  	   	          *  	   6     @     H     Q     X     s                                                                               $   
   !         	       #   &              %      "                                                      A small collection of lightweight WordPress blocks that can accomplish nearly anything. Active websites Angle %s Angles Buttons Buttons Documentation CSS files regenerated. Container Container Documentation Craft text-rich content with advanced typography. Create advanced layouts with flexible grids. Curve %s Curves Dashboard Documentation Downloads Drive conversions with beautiful buttons. GenerateBlocks Grid Grid Documentation Happy customers Headline Headline Documentation Learn More Learn more Looking for a WordPress theme? GenerateBlocks and GeneratePress are built with the same principles in mind and complement each other perfectly. No changes found. Organize your content into rows and sections. Settings Settings saved. Tom Usborne Triangle %s Triangles Wave %s Waves https://generateblocks.com https://tomusborne.com PO-Revision-Date: 2022-01-01 08:59:59+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fi
Project-Id-Version: Plugins - GenerateBlocks - Stable (latest release)
 Kokoelma keveitä WordPress -lohkoja, joiden avulla voit tehdä melkein mitä vaan. Sivustot, joissa on käytössä Kulma %s Kulmat Painikkeet Painikkeen käytön ohjeet CSS -tiedostot luotu uudestaan. Säiliö Säiliön käytön ohjeet Tee käsityönä hyvännäköistä tekstisisältöä edistyneellä typografialla. Luo monipuolisia asetteluja mukautuvilla ruudukoilla. Kaari %s Kaaret Ohjausnäkymä Ohjeet Lataukset Parempia tuloksia konversioihin kauniiden painikkeiden avulla. GenerateBlocks Ruudukko Ruudukon käytön ohjeet Tyytyväisiä asiakkaita Otsikko Otsikon käytön ohjeet Lue lisää Lue ja opi lisää Etsitkö WordPress-teemaa? GenerateBlocks ja GeneratePress on rakennettu samojen periaatteiden mukaisesti ja ne täydentävät toisiaan täydellisesti. Ei löydettyjä muutoksia. Järjestä sisältösi riveihin ja alueisiin. Asetukset Asetukset tallennettu. Tom Usborne Kolmio %s Kolmiot Aalto %s Aallot https://generateblocks.com https://tomusborne.com 