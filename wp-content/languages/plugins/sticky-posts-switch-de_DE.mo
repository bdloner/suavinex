��    (      \  5   �      p     q  $   �  <   �     �  
   �  <   �     <     B     J     S     X  /   h     �     �     �     �     �  
   �     �          	  
     	   &  6   0     g     p  -   u  !   �     �     �     �     �            3   "  b   V     �  "   �      �  0       4  (   T  M   }     �  
   �  l   �     I	     O	  
   W	     b	     h	  8   x	     �	     �	     �	     �	     �	     
     #
     /
     5
     G
  
   T
  H   _
     �
     �
  >   �
  !   �
           ,  *   @     k     �     �  +   �  �   �     ^  "   d  
   �                                   '                 (                      "      
      !      $       #         &                                     	   %                            &mdash; No Change &mdash; Active multilingual plugin found: %s An error has occurred. Please reload the page and try again. Author Categories Change the display options for the admin sticky post column. Color Columns Comments Date Display Options Handle the post selection of custom post types. Homepage Make this post sticky Markus Wiesenhofer Multilingual posts Multilingual settings Not Sticky Order Page Post Type Archive Post Types Post type Set all the translations sticky, when one post is set. Settings Show Sorry, you are not allowed to edit this item. Stick this post to the front page Sticky Sticky Post Sticky Post Settings Sticky Posts - Switch Supported Plugins: %s, %s Tags There are no supported multilingual plugins active. This plugin adds a sticky post switch functionality to the admin list post/custom post type pages. Title mailto:markusfroehlich01@gmail.com taxonomy general nameCategories PO-Revision-Date: 2022-01-28 00:12:33+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: de
Project-Id-Version: Plugins - Sticky Posts &#8211; Switch - Development (trunk)
 &mdash; Keine Änderung &mdash; Aktives Mehrsprachen Plugin gefunden: %s Ein Fehler ist aufgetreten. Bitte lade die Seite neu und versuche es nochmal. Autor Kategorien Ändere die Anzeigeoptionen der Beitrag oben halten Spalte in deiner Beitrags/Eigener Inhaltstyp Übersicht. Farbe Spalten Kommentare Datum Anzeigeoptionen Anpassung der Beitrags Ausgabe von eigenen Inhaltstypen. Homepage Beitrag oben halten Markus Wiesenhofer Mehrsprachige Beiträge Mehrsprachige Beiträge Nicht oben halten Reihenfolge Seite Inhaltstyp Archiv Inhaltstypen Inhaltstyp Halte alle mehrsprachigen Beiträge oben, wenn ein Beitrag gesetzt wird. Einstellungen Anzeigen Du bist leider nicht berechtigt, dieses Element zu bearbeiten. Beitrag auf der Startseite halten Oben halten Beitrag oben halten Einstellungen &rsaquo; Beitrag oben halten Sticky Posts - Switch Unterstützte Plugins: %s, %s Schlagwörter Es sind keine Unterstützten Plugins aktiv. Dieses Plugin fügt die Beitrag oben halten Funktion als Icon-Schalter in deiner Admin Beitrags/Eigener Inhaltstyp Übersicht hinzu. Titel mailto:markusfroehlich01@gmail.com Kategorien 