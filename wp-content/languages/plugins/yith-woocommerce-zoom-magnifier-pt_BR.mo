��    4      �  G   \      x     y     �  
   �     �  6   �     �       	     '         H     e     m     t  %   �     �     �  
   �     �       J        W     ^     k     x  
   �     �  
   �     �     �     �     �     �     �     �               !     /     =     J     \     e     t     �     �     �  >   �  H        W     ^     d  L  j     �	  &   �	     �	     
  8   "
     [
     n
     �
  "   �
  ,   �
     �
     �
     �
  '   �
  &   &  '   M     u     �     �  Y   �               "     6     L     Z     _     k     ~     �     �     �     �  	   �     �     �     �          #     4     I  %   Y       +   �     �  %   �  J   	  =   T     �     �     �                           (               "   $         1          -       0   )          *   #   .   '                                    	                    +                   ,   4         
   3                 !   /   %               2           &    Add Images to Gallery Add custom product tab Add images Add to gallery An error has occurred during import. Please try again. Are you sure? Choose a sidebar Close all Content of the tab. (HTML is supported) Database imported correctly. Default Delete Delete image Do you want to remove the custom tab? Element deleted correctly. Element updated correctly. Expand all How to install premium version Icon If you continue with this action, you will reset all options in this page. Inside Left Sidebar Left sidebar Loading label Loading... Name No sidebar Plugin Settings Plugins Activated Plugins Upgraded Premium Version Premium version Remove Reset Reset Defaults Reset to default Right Sidebar Right sidebar Save Changes Search Categories Settings Settings reset Settings saved Sorry, import is disabled. Sorting successful. The added file is not valid. The changes you have made will be lost if you leave this page. The element you have entered already exists. Please, enter another name. Upload Value Width PO-Revision-Date: 2018-11-30 17:31:43+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: pt_BR
Project-Id-Version: Plugins - YITH WooCommerce Product Gallery &amp; Image Zoom - Stable (latest release)
 Adicionar imagens à galeria Adicionar aba personalizada do produto Adicionar imagens Adicionar à galeria Ocorreu um erro durante a importação. Tente novamente. Você tem certeza? Escolha uma lateral Fechar todos Conteúdo da aba. (pode usar HTML) O banco de dados foi importado corretamente. Padrão Excluir Excluir imagem Você quer remover a aba personalizada? O elemento foi excluído corretamente. O elemento foi atualizado corretamente. Expandir todos Como instalar a versão premium Ãcone Se você continuar com esta ação, você irá redefinir todas as opções nesta página. Dentro Lateral à esquerda Lateral à esquerda Texto de carregamento Carregando… Nome Sem lateral Opções do plugin Plugins ativados Plugins atualizados Versão Premium Versão Premium Remover Redefinir Restaurar padrões Restaurar ao padrão Lateral à direita Lateral à direita Salvar mudanças Pesquisar categorias Configurações As configurações foram redefinidas. As configurações foram salvas Lamento, a importação está desabilitada. Ordenação concluída. O arquivo adicionado não é válido. As alterações que você fez serão perdidas se você sair desta página. O elemento que você informou já existe. Escolha outro nome. Enviar Valor Largura 