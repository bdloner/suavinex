��          �   %   �      0  W   1     �     �  	   �  1   �  ,   �  	   
       	   "  )   ,     V     e     j     z  
   �  
   �  �   �  -   )     W     `     p     |     �  )  �  [   �     4     A  
   I  5   T  .   �     �     �  	   �  #   �     	               /  
   7  
   B  �   M  ,   �          %     =     I     d                                                     
   	                                                                       A small collection of lightweight WordPress blocks that can accomplish nearly anything. Active websites Buttons Container Craft text-rich content with advanced typography. Create advanced layouts with flexible grids. Dashboard Documentation Downloads Drive conversions with beautiful buttons. GenerateBlocks Grid Happy customers Headline Learn More Learn more Looking for a WordPress theme? GenerateBlocks and GeneratePress are built with the same principles in mind and complement each other perfectly. Organize your content into rows and sections. Settings Settings saved. Tom Usborne https://generateblocks.com https://tomusborne.com PO-Revision-Date: 2020-08-22 19:19:11+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: pt_BR
Project-Id-Version: Plugins - GenerateBlocks - Stable (latest release)
 Uma pequena coleção de blocos leves do WordPress que podem realizar quase qualquer coisa. Sites ativos Botões Contêiner Crie conteúdo textual rico com tipografia avançada. Crie layouts avançados com grades flexíveis. Painel de controle Documentação Downloads Gere conversões com belos botões. GenerateBlocks Grade Clientes felizes Título Saiba mais Saiba mais Procurando um tema para o WordPress? GenerateBlocks e GeneratePress são construídos com os mesmos princípios em mente e se complementam perfeitamente. Organize seu conteúdo em linhas e seções. Configurações  Configurações salvas. Tom Usborne https://generateblocks.com https://tomusborne.com 