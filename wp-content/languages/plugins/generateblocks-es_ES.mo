��    &      L  5   |      P  W   Q     �     �     �     �     �     �  	   �       1      ,   R          �  	   �     �  	   �  )   �     �     �     �                 
   2  
   =  �   H     �  -   �          !     1     =  	   I     S     [     a     |  '  �  `   �       
   )     4     =     E  #   c  
   �     �  6   �  3   �     	     $	  
   +	     6	  	   E	  )   O	     y	     �	      �	     �	     �	     �	     �	     �	  �   
     �
  +   �
     �
     �
               (     4     <     B     ]                                                                               $   
   !         	       #   &              %      "                                                      A small collection of lightweight WordPress blocks that can accomplish nearly anything. Active websites Angle %s Angles Buttons Buttons Documentation CSS files regenerated. Container Container Documentation Craft text-rich content with advanced typography. Create advanced layouts with flexible grids. Curve %s Curves Dashboard Documentation Downloads Drive conversions with beautiful buttons. GenerateBlocks Grid Grid Documentation Happy customers Headline Headline Documentation Learn More Learn more Looking for a WordPress theme? GenerateBlocks and GeneratePress are built with the same principles in mind and complement each other perfectly. No changes found. Organize your content into rows and sections. Settings Settings saved. Tom Usborne Triangle %s Triangles Wave %s Waves https://generateblocks.com https://tomusborne.com PO-Revision-Date: 2021-10-27 07:48:22+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: es
Project-Id-Version: Plugins - GenerateBlocks - Stable (latest release)
 Una pequeña colección de bloques ligeros para WordPress que pueden lograr casi cualquier cosa. Webs activas Ángulo %s Ángulos Botones Documentación de los botones Los archivos CSS se han regenerado. Contenedor Documentación del contenedor Crea contenido rico en texto con tipografía avanzada. Crea diseños avanzados con cuadrículas flexibles. Curva %s Curvas Escritorio Documentación Descargas Genera conversiones con hermosos botones. GenerateBlocks Cuadrícula Documentación de la cuadrícula Clientes satisfechos Titular Documentación del encabezado Aprende más Aprender más ¿Buscas un tema para WordPress? GenerateBlocks y GeneratePress están creados pensando en los mismos principios y se complementan perfectamente entre ellos. No se han encontrado cambios. Organiza tu contenido en filas y secciones. Ajustes Ajustes guardados. Tom Usborne Triángulo %s Triángulos Onda %s Ondas https://generateblocks.com https://tomusborne.com 