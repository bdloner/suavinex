��    m      �  �   �      @	  c   A	  +   �	     �	     �	  	   �	  
   �	     �	     
     
     (
     9
     M
     _
     o
     x
     �
     �
     �
  
   �
     �
     �
     �
  
   �
     �
     �
     �
       W        l     t     z     �     �     �     �     �     �     �     �  	     
             "     /     6     P     X     g     u     }  ]   �     �     �                 	   +     5     F     U  	   a     k  
   y  
   �     �     �     �  	   �     �     �  
   �     �               	       
        $     8     J     P     ]  	   j     t     {     �     �     �     �     �     �  
   �     �     �  +   �     +  5   /     e     t     �     �  	   �  	   �     �     �  �   �     w     �  6  �  _   �  1   $     V     f     r     �     �     �     �     �     �     �     �               $     ,     ;     @     Z     a     t     �  
   �     �     �     �  l   �     =     E  	   L     V     k     z     �     �     �     �     �     �     �     �          "     )  	   G     Q     b     q     x  T   �     �     �               1     L     T     c     x     �     �     �  
   �     �     �     �     �     	        
   &     1     C     G     J     [  
   a     l          �     �     �     �     �     �     �     �       
        '     /     D     T     e  2   z     �  I   �     �     	          ,     A     S     c     j  �        2     D        "   
       U   7      A          Y   /          ]      [   =   ,   g       )   >      %       X           i       H   K           M   k   .   -       V          4           8   N   1   !   ^   ;   *       J   \   +       R           G   d   f       T                       6   5   @   P                  m      B           :                  (   F   $   I              D           #       h   9         c              e           l   `   b   E   _          2      j   Q   Z   3   '              W      0   <   C          L          	   ?   O   &   S       a    "output" invalid format in field %s. The "output" argument should be defined as an array of arrays. "partial_refresh" invalid entry in field %s %1$s or %2$s Add File Add Image Add new %s Auto Background Attachment Background Color Background Image Background Position Background Repeat Background Size Bold 700 Bold 700 Italic Book 300 Book 300 Italic Bottom Capitalize Center Center Bottom Center Center Center Top Change File Change Image Change image Choose image Config not defined for field %1$s - See %2$s for details on how to properly add fields. Contain Cover Customizing Customizing &#9656; %s David Vongries Default Don't show this again Extra-Bold 800 Extra-Bold 800 Italic Fixed Font Family Font Size Font Style Font Weight Google Fonts Height Important Upgrade Notice: Initial Install Plugin Invalid Value Justify Kirki Customizer Framework Kirki fields should not be added on customize_register. Please add them directly, or on init. Left Left Bottom Left Center Left Top Letter Spacing Light 200 Light 200 Italic Limit: %s rows Line Height Lowercase Margin Bottom Margin Top Medium 500 Medium 500 Italic No File Selected No Image Selected No Repeat No image selected None Normal 400 Normal 400 Italic Off On Overline Remove Repeat All Repeat Horizontally Repeat Vertically Right Right Bottom Right Center Right Top Scroll Select a Page Select image Semi-Bold 600 Semi-Bold 600 Italic Size Spacing Standard Fonts Text Align Text Decoration Text Transform The Ultimate WordPress Customizer Framework Top Typo found in field %s - setting instead of settings. Ultra-Bold 900 Ultra-Bold 900 Italic Ultra-Light 100 Ultra-Light 100 Italic Underline Uppercase Width Word Spacing Your theme uses a Font Awesome field for icons. To avoid issues with missing icons on your frontend we recommend you install the official Font Awesome plugin. https://kirki.org row PO-Revision-Date: 2020-06-30 15:05:26+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: da_DK
Project-Id-Version: Plugins - Kirki Customizer Framework - Stable (latest release)
 Ugyldig "output" format i feltet %s. "Output" argumentet bør defineres som et array af arrays. Ugyldig "partial_refresh" indtastning i feltet %s %1$s eller %2$s Tilføj fil Tilføj billede Tilføj ny %s Auto Baggrunds vedhæftning Baggrunds farve Baggrunds billede Baggrunds position Gentagelse af baggrund Baggrunds størrelse Fed 700 Fed 700 Kursiv Bog 300 Bog 300 Kursiv Bund Stort begyndelses bogstav Midten Centrer ved bunden Centrer i midten Centrer ved toppen Ændre fil Ændre billede Udskift billede Vælg billede Config er ikke difineret for feltet %1$s - Se %2$s for oplysninger om hvordan feltet korrekt skal tilføjes. Indhold Dække Tilpasser Tilpasser &#9656; %s David Vongries Standard Vis ikke dette igen Ekstra-Fed 800 Ekstra-Fed 800 Kursiv Fikseret Skrifttype familie Skrifttype størrelse Skrifttype stil Skrifttype vægt Google Skrifttyper Højde Vigtig opgraderings meddelse: Initialer Installer plugin Ugyldig værdi Juster Kirki Customizer Framework Kirki felterne bør ikke tilføjes customize_register. Tilføj dem venligst direkte. Venstre Til venstre ved bunden Til venstre i midten Til venstre i toppen Mellemrum mellem bogstaver Let 200 Let 200 Kursiv Begræns: %s rækker Linje højde Små bogstaver Margin - bund Margin - top Medium 500 Medium 500 Kursiv Ingen fil blev valgt Ingen billede er valgt Ingen gentagelse Intet billede er valgt Ingen Normal 400 Normal 400 Kursiv Off On Linje over tekst Fjern Gentag alt Gentag horisontalt Gentag vertikalt Højre Til højre ved bunden Til højre i midten Til højre ved toppen Rulning Vælg en side Vælg billede Halv-Fed 600 Halv-Fed 600 Kursiv Størrelse Afstand Standard skrifttyper Tekst justering Tekst dekoration Tekst transformering Det Ultimative WordPress Brugerdefineret Framework Top Stavefejl er fundet i feltet %s - indstilling i stedet for indstillinger. Ultra-Fed 900 Ultra-Fed 900 Kursiv Ultra-Let 100 Ultra-Let 100 Kursiv Linje under tekst Store bogstaver Bredde Mellemrum mellem ord Dit tema anvender et Font Awesome felt til ikoner. For at undgå problemer med manglende ikoner på din forside, anbefaler vi, at du installer det officielle Font Awesome plugin. https://kirki.org række 