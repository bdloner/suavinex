��    &      L  5   |      P  W   Q     �     �     �     �     �     �  	   �       1      ,   R          �  	   �     �  	   �  )   �     �     �     �                 
   2  
   =  �   H     �  -   �          !     1     =  	   I     S     [     a     |  '  �  V   �                (     /     7     L  	   i     s  3   �  4   �     �     �  	   	     	  	   	  +   %	     Q	     `	     g	     {	     �	     �	  	   �	  	   �	  �   �	     P
  )   k
     �
     �
     �
     �
  
   �
     �
     �
     �
                                                                                    $   
   !         	       #   &              %      "                                                      A small collection of lightweight WordPress blocks that can accomplish nearly anything. Active websites Angle %s Angles Buttons Buttons Documentation CSS files regenerated. Container Container Documentation Craft text-rich content with advanced typography. Create advanced layouts with flexible grids. Curve %s Curves Dashboard Documentation Downloads Drive conversions with beautiful buttons. GenerateBlocks Grid Grid Documentation Happy customers Headline Headline Documentation Learn More Learn more Looking for a WordPress theme? GenerateBlocks and GeneratePress are built with the same principles in mind and complement each other perfectly. No changes found. Organize your content into rows and sections. Settings Settings saved. Tom Usborne Triangle %s Triangles Wave %s Waves https://generateblocks.com https://tomusborne.com PO-Revision-Date: 2022-03-18 14:59:31+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - GenerateBlocks - Stable (latest release)
 Een kleine verzameling lichtgewicht WordPress blokken die bijna alles kunnen bereiken. Actieve sites Hoek %s Hoeken Knoppen Knoppen documentatie CSS bestanden geregenereerd. Container Container documentatie Maak tekstrijke inhoud met geavanceerde typografie. Creëer geavanceerde lay-outs met flexibele rasters. Bocht %s Bochten Dashboard Documentatie Downloads Stimuleer conversies met prachtige knoppen. GenerateBlocks Raster Raster documentatie Tevreden klanten Koptekst Kopregel documentatie Lees meer Leer meer Op zoek naar een WordPress-thema? GenerateBlocks en GeneratePress zijn gebouwd met dezelfde principes in gedachten en vullen elkaar perfect aan. Geen wijzigingen gevonden. Organiseer je inhoud in rijen en secties. Instellingen Instellingen opgeslagen. Tom Usborne Driehoek %s Driehoeken Golf %s Golven https://generateblocks.com https://tomusborne.com 