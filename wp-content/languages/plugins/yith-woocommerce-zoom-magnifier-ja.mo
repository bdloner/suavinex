��    4      �  G   \      x     y     �  
   �     �  6   �     �       	     '         H     e     m     t  %   �     �     �  
   �     �       J        W     ^     k     x  
   �     �  
   �     �     �     �     �     �     �     �               !     /     =     J     \     e     t     �     �     �  >   �  H        W     ^     d  H  j  !   �	  !   �	     �	     
  Z    
     {
     �
     �
  0   �
  <   �
     3     C     J  )   Z     �     �     �  -   �        c        q     x     �  !   �     �     �     �     �       $   .     S     r     �     �     �     �     �     �     �          &     -     C  *   S     ~  <   �  B   �  `        u     �     �                           (               "   $         1          -       0   )          *   #   .   '                                    	                    +                   ,   4         
   3                 !   /   %               2           &    Add Images to Gallery Add custom product tab Add images Add to gallery An error has occurred during import. Please try again. Are you sure? Choose a sidebar Close all Content of the tab. (HTML is supported) Database imported correctly. Default Delete Delete image Do you want to remove the custom tab? Element deleted correctly. Element updated correctly. Expand all How to install premium version Icon If you continue with this action, you will reset all options in this page. Inside Left Sidebar Left sidebar Loading label Loading... Name No sidebar Plugin Settings Plugins Activated Plugins Upgraded Premium Version Premium version Remove Reset Reset Defaults Reset to default Right Sidebar Right sidebar Save Changes Search Categories Settings Settings reset Settings saved Sorry, import is disabled. Sorting successful. The added file is not valid. The changes you have made will be lost if you leave this page. The element you have entered already exists. Please, enter another name. Upload Value Width PO-Revision-Date: 2017-07-21 02:16:54+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: GlotPress/3.0.0-alpha.2
Language: ja_JP
Project-Id-Version: Plugins - YITH WooCommerce Product Gallery &amp; Image Zoom - Stable (latest release)
 ギャラリーに画像を追加 カスタム商品タブを追加 画像を追加 ギャラリーに追加 インポート中にエラーが発生しました。もう一度お試しください。 本当に実行しますか ? サイドバーを選択 すべて閉じる タブのコンテンツ (HTML 利用可能) 。 データベースのインポートを完了しました。 デフォルト 削除 画像を削除 カスタムタブを削除しますか ? 要素を削除しました。 要素を更新しました。 すべて展開 プレミアム版のインストール方法 アイコン 操作を続行すると、このページのオプションがすべてリセットされます。 内側 左サイドバー 左サイドバー ローディング中のラベル 読み込んでいます... 名前 サイドバーなし プラグイン設定 プラグイン有効化済み プラグインを更新しました プレミアムバージョン プレミアムバージョン 削除 リセット デフォルトに戻す デフォルトに戻す 右サイドバー 右サイドバー 変更を保存 カテゴリーを検索 設定 設定をリセット 設定を保存 インポート機能は停止中です。 並べ替え完了。 追加されたファイルは有効ではありません。 このページから離れると変更内容は失われます。 入力した要素がすでに存在しています。他の名前を入力してください。 アップロード 値 幅 