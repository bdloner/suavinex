��    m      �  �   �      @	  c   A	  +   �	     �	     �	  	   �	  
   �	     �	     
     
     (
     9
     M
     _
     o
     x
     �
     �
     �
  
   �
     �
     �
     �
  
   �
     �
     �
     �
       W        l     t     z     �     �     �     �     �     �     �     �  	     
             "     /     6     P     X     g     u     }  ]   �     �     �                 	   +     5     F     U  	   a     k  
   y  
   �     �     �     �  	   �     �     �  
   �     �               	       
        $     8     J     P     ]  	   j     t     {     �     �     �     �     �     �  
   �     �     �  +   �     +  5   /     e     t     �     �  	   �  	   �     �     �  �   �     w     �  1  �  T   �  +        @     N     [  	   h     r     y     �     �     �     �     �  
   �     �  
   �     �                         ,     3     @     M     Z     g  b   t     �     �     �     �     
  	        #  
   0     ;     M  	   T     ^     k     x     �     �     �     �     �  	   �     �     �  ]   �     S  	   W     a     n     u  
   �     �     �     �     �     �     �  	   �     �     �       	             /     3     7     I     M  	   Q     [     b     o     |     �     �     �     �     �     �     �  	   �     �     �     �     �               ,  !   9     [  E   _  
   �     �  
   �     �  	   �     �     �  	   �  �        �     �        "   
       U   7      A          Y   /          ]      [   =   ,   g       )   >      %       X           i       H   K           M   k   .   -       V          4           8   N   1   !   ^   ;   *       J   \   +       R           G   d   f       T                       6   5   @   P                  m      B           :                  (   F   $   I              D           #       h   9         c              e           l   `   b   E   _          2      j   Q   Z   3   '              W      0   <   C          L          	   ?   O   &   S       a    "output" invalid format in field %s. The "output" argument should be defined as an array of arrays. "partial_refresh" invalid entry in field %s %1$s or %2$s Add File Add Image Add new %s Auto Background Attachment Background Color Background Image Background Position Background Repeat Background Size Bold 700 Bold 700 Italic Book 300 Book 300 Italic Bottom Capitalize Center Center Bottom Center Center Center Top Change File Change Image Change image Choose image Config not defined for field %1$s - See %2$s for details on how to properly add fields. Contain Cover Customizing Customizing &#9656; %s David Vongries Default Don't show this again Extra-Bold 800 Extra-Bold 800 Italic Fixed Font Family Font Size Font Style Font Weight Google Fonts Height Important Upgrade Notice: Initial Install Plugin Invalid Value Justify Kirki Customizer Framework Kirki fields should not be added on customize_register. Please add them directly, or on init. Left Left Bottom Left Center Left Top Letter Spacing Light 200 Light 200 Italic Limit: %s rows Line Height Lowercase Margin Bottom Margin Top Medium 500 Medium 500 Italic No File Selected No Image Selected No Repeat No image selected None Normal 400 Normal 400 Italic Off On Overline Remove Repeat All Repeat Horizontally Repeat Vertically Right Right Bottom Right Center Right Top Scroll Select a Page Select image Semi-Bold 600 Semi-Bold 600 Italic Size Spacing Standard Fonts Text Align Text Decoration Text Transform The Ultimate WordPress Customizer Framework Top Typo found in field %s - setting instead of settings. Ultra-Bold 900 Ultra-Bold 900 Italic Ultra-Light 100 Ultra-Light 100 Italic Underline Uppercase Width Word Spacing Your theme uses a Font Awesome field for icons. To avoid issues with missing icons on your frontend we recommend you install the official Font Awesome plugin. https://kirki.org row PO-Revision-Date: 2021-04-01 07:49:40+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: GlotPress/3.0.0-alpha.2
Language: zh_CN
Project-Id-Version: Plugins - Kirki Customizer Framework - Stable (latest release)
 字段%s中的"output"格式无效。"output"参数应该被定义为一个数组。 字段%s中有无效项目"partial_refresh" %1$s 或 %2$s 添加文件 添加图片 新增 %s 自动 背景附着方式 背景颜色 背景图像 背景位置 重复背景 背景尺寸 粗体 700 黑 700 斜体 一般 300 一般 300 斜体 下 首字母大写 居中 底部居中 中心 中心顶部 更改文件 更改图片 更改图像 选择图像 没有为字段%1$s定义配置 - 有关如何正确添加字段的详细信息，请参阅%2$s。 包含 封面 正在自定义 自定义 &#9656; %s David Vongries 默认值 不再显示 特粗 800 特粗 800 斜体 固定 字体源 字体尺寸 字体样式 字体重量 Google字体 高度 重要更新通知： 初始 安装插件 无效值 左右对齐 Kirki Customizer Framework Kirki字段不应添加到custom_register上。请直接添加它们，或在init上添加。 左 左下方 左侧局中 左上 文字间距 浅细 200 细 200 斜体 限制：%s行 行高 小写 底部边距 顶部边距 中等500 中等 500 斜体 没有选择文件 没有选择图像 不重复 未选择图像 无 400 正常 400 斜体 关 开 上划线 移除 重复全部 水平重复 垂直重复 右 右侧底部 右侧局中 右侧顶部 滚动 选择页面 选择图像 加粗600 半粗 600 斜体 大小 间距 标准字体 文字对齐方式 文本修饰 文字转变 终极WordPress定制工具框架 上 在字段%s中发现拼写错误-应该时setting而不是settings。 特粗 900 特粗 900 斜体 超细 100 超细 100 斜体 下划线 大写 宽度 字间距 您的主题使用Font Awesome字段作为图标。为了避免前端缺少图标的问题，建议您安装官方Font Awesome插件。 https://kirki.org 列 