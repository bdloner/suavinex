��    l      |  �   �      0	  c   1	  +   �	     �	     �	  	   �	  
   �	     �	     �	     
     
     )
     =
     O
     _
     h
     x
     �
     �
  
   �
     �
     �
     �
  
   �
     �
     �
     �
     �
  W        \     d     j     v     �     �     �     �     �     �     �  	   �  
   �                    &     @     H     W     e     m  ]   �     �     �     �            	        %     6     E  	   Q     [  
   i  
   t          �     �  	   �     �     �  
   �     �     �     �     �       
   	          (     :     @     M  	   Z     d     k     y     �     �     �     �     �  
   �     �     �  +   �       5        U     d     z     �  	   �  	   �     �     �     �     �  6  �  j     .   �     �     �     �     �     �     �                     0     D     X     a     q     z     �  
   �     �     �     �     �     �     �  
   �  
   �  `   �     S     \  	   b     l     �     �     �     �     �     �     �     �  	   �                    &     B     H     \     j     v  j   �     �                     -  	   <     F     W     h     t     �     �  
   �     �     �     �     �     �     �  
                   #  	   '     1     7     D     X     j     q     }     �     �     �  
   �     �     �  
   �     �     �     �     
       2   /     b  <   g     �     �     �     �  
   �     �       
             /        "   
       U   7      A          Y   /          ]      [   =   ,   g       )   >      %       X           i       H   K           M       .   -       V          4           8   N   1   !   ^   ;   *       J   \   +       R           G   d   f       T                       6   5   @   P                  l      B           :                  (   F   $   I              D           #       h   9         c              e           k   `   b   E   _          2      j   Q   Z   3   '              W      0   <   C          L          	   ?   O   &   S       a    "output" invalid format in field %s. The "output" argument should be defined as an array of arrays. "partial_refresh" invalid entry in field %s %1$s or %2$s Add File Add Image Add new %s Auto Background Attachment Background Color Background Image Background Position Background Repeat Background Size Bold 700 Bold 700 Italic Book 300 Book 300 Italic Bottom Capitalize Center Center Bottom Center Center Center Top Change File Change Image Change image Choose image Config not defined for field %1$s - See %2$s for details on how to properly add fields. Contain Cover Customizing Customizing &#9656; %s David Vongries Default Don't show this again Extra-Bold 800 Extra-Bold 800 Italic Fixed Font Family Font Size Font Style Font Weight Google Fonts Height Important Upgrade Notice: Initial Install Plugin Invalid Value Justify Kirki Customizer Framework Kirki fields should not be added on customize_register. Please add them directly, or on init. Left Left Bottom Left Center Left Top Letter Spacing Light 200 Light 200 Italic Limit: %s rows Line Height Lowercase Margin Bottom Margin Top Medium 500 Medium 500 Italic No File Selected No Image Selected No Repeat No image selected None Normal 400 Normal 400 Italic Off On Overline Remove Repeat All Repeat Horizontally Repeat Vertically Right Right Bottom Right Center Right Top Scroll Select a Page Select image Semi-Bold 600 Semi-Bold 600 Italic Size Spacing Standard Fonts Text Align Text Decoration Text Transform The Ultimate WordPress Customizer Framework Top Typo found in field %s - setting instead of settings. Ultra-Bold 900 Ultra-Bold 900 Italic Ultra-Light 100 Ultra-Light 100 Italic Underline Uppercase Width Word Spacing https://kirki.org row PO-Revision-Date: 2020-05-14 13:35:58+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nb_NO
Project-Id-Version: Plugins - Kirki Customizer Framework - Stable (latest release)
 "output" ugyldig format i felt %s. "output"-argumentet burde være definert som en rekke med flere rekker. "partial_refresh" ugyldig oppføring i felt %s %1$s eller %2$s Legg til fil Legg til bilde Legg til ny %s Auto Bakgrunnsfesting Bakgrunnsfarge Bakgrunnsbilde Bakgrunnsposisjon Bakgrunnsrepetisjon Bakgrunnsstørrelse Bold 700 Bold 700 Italic Book 300 Book 300 Italic Bunn Kapitéler Midten Midten bunn Midten midten Midten topp Bytt fil Endre bilde Bytt bilde Velg bilde Konfigurasjon ikke definert for felt %1$s - Se %2$s for detaljer om hvordan man legger til felt. Innehold Dekke Tilpasser Tilpasser &#9656; %s David Vongries Standard Ikke vis dette igjen Extra-Bold 800 Extra-Bold 800 Italic Festet Font familie Font størrelse Font stil Font tykkelse Google-skrifter Høyde Viktig oppgraderingsvarsel: Start Installer utvidelse Ugyldig verdi Blokkjuster Kirki Customizer Framework Kirki-felt burde ikke bli lagt til på customize_register. Vennligst legg dem til direkte, eller på init. Venstre Venstre bunn Venstre midten Venstre topp Bokstavavstand Light 200 Light 200 Italic Grense: %s rader Linjehøyde Små bokstaver Margin bunn Margin topp Medium 500 Medium 500 Italic Ingen fil valgt Ingen bilde valgt Ingen repetisjon Intet bilde valgt Ingen Normal 400 Normal 400 Italic Av På Overstrek Slett Repeter alle Repeter horisontalt Repeter vertikalt Høyre Høyre bunn Høyre midten Høyre topp Rull Velg en side Velg bilde Semi-Bold 600 Semi-Bold 600 Italic Størrelse Avstand Standardskrifter Tekstjustering Tekstdekorasjon Tekst-transformering Det ultimate rammeverket for WordPress-tilpasseren Topp Skrivefeil funnet i felt %s - setting i stedet for settings. Ultra-Bold 900 Ultra-Bold 900 Italic Ultra-Light 100 Ultra-Light 100 Italic Understrek Store bokstaver Bredde Ordavstand https://kirki.org rad 