��    �      �  �   ,	      0  c   1     �     �  
   �     �  
   �     �  
   �     �  
   �     �  
   �     �  
   �     �  
           
          	   #  
   -  
   8     C     H     ^     o     �     �     �     �     �     �     �     �  
   �     �            
        (     4     A     N  W   [     �     �     �     �     �     �     �               )     ?  
   E     P  	   \  
   f     q     ~     �     �     �     �     �     �     �     �     �     �  ]        s     x     �     �     �  	   �     �     �     �     �  	   �     �  
          
             1     B  	   T     ^     p  
   u     �     �     �     �     �     �     �     �     �     �  
   �     �     �                 	   ,     6     =     K  	   X     b     p     �     �     �     �     �     �  
   �  
   �     �     �     �  5   �     %     4     J     Z  	   q  	   {     �     �     �     �     �  �   �     S     n     �  .  �  o   �     #     3  
   7     B  
   F     Q  
   U     `  
   d     o  
   s     ~  
   �     �  
   �     �  
   �     �     �     �  	   �     �     �     �               .     E     V     ^     m     y     �     �  	   �     �     �     �  
   �     �     �  
     ~        �     �     �     �     �     �     �     �     �     �       	        !     1     =     K     \     j     w     }      �     �     �     �     �     �     �  _        c     l     |     �     �  	   �     �     �     �     �     �     �            
   !     ,     >     M     ]     n     ~  
   �     �     �     �     �     �     �     �  	   �     �     �     �     �          &     -     ;     L     Z     `  
   n  	   y     �     �     �     �     �  	   �  	   �     �  
   �     �     �            A        ]     j     ~     �     �     �     �     �     �     �     �  �   �     �     �     �            K   7       T              /   �   t   Q         y   P   F       k           c   �       I   �   e       !       �   ~   n                  R   r   �   U                  6   ]   ^   -       a       3       S         �      {   s             .       j   p   h                   �   4   Y   w   &       |   V   9   \   v   (       �   �      :       E   A       G   '      [   m      
   C   B   �         #          )           "   2   z   �       �       �   =       D   ,           f       1   �   i   X   o   >      d   _   l   �       �   <      `       �      b   5   Z   @   ;   *       %   H      g         $   L   	      �      +      q          0   }   u   M   x       ?               W       J   O             N   8        "output" invalid format in field %s. The "output" argument should be defined as an array of arrays. %1$s or %2$s 100 100 Italic 200 200 Italic 300 300 Italic 500 500 Italic 600 600 Italic 700 700 Italic 800 800 Italic 900 900 Italic Add File Add Image Add new %s Admin Menu Auto Background Attachment Background Color Background Image Background Position Background Repeat Background Size Bold 700 Bold 700 Italic Book 300 Book 300 Italic Bottom Capitalize Center Center Bottom Center Center Center Top Change File Change Image Change image Choose image Config not defined for field %1$s - See %2$s for details on how to properly add fields. Contain Cover Customizing Customizing &#9656; %s David Vongries Default Defaults Don't show this again Extra-Bold 800 Extra-Bold 800 Italic Fixed Font Color Font Family Font Size Font Style Font Variant Font Weight Google Fonts Height Image Editing Important Upgrade Notice: Initial Install Plugin Invalid Value Italic Justify Kirki Customizer Framework Kirki fields should not be added on customize_register. Please add them directly, or on init. Left Left Bottom Left Center Left Top Letter Spacing Light 200 Light 200 Italic Limit: %s rows Line Height Line Through Lowercase Margin Bottom Margin Top Media Medium 500 Medium 500 Italic No File Selected No Image Selected No Repeat No image selected None Normal 400 Normal 400 Italic Notifications Off On Overline Post Formats Posts Products Regular Remove Repeat All Repeat Horizontally Repeat Vertically Right Right Bottom Right Center Right Top Scroll Select a Page Select image Select... Semi-Bold 600 Semi-Bold 600 Italic Size Social Solid Sorting Spacing Standard Fonts Taxonomies Text Align Text Decoration Text Transform Top Typo found in field %s - setting instead of settings. Ultra-Bold 900 Ultra-Bold 900 Italic Ultra-Light 100 Ultra-Light 100 Italic Underline Uppercase Wavy Welcome Screen Widgets Width Word Spacing Your theme uses a Font Awesome field for icons. To avoid issues with missing icons on your frontend we recommend you install the official Font Awesome plugin. https://davidvongries.com/ https://kirki.org row PO-Revision-Date: 2022-04-18 14:53:25+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0
Language: sv_SE
Project-Id-Version: Plugins - Kirki Customizer Framework - Stable (latest release)
 ”output” är ett ogiltigt format i fältet %s. Argumentet ”output” måste anges som en array av arrays. %1$s eller %2$s 100 100 kursiv 200 200 kursiv 300 300 kursiv 500 500 kursiv 600 600 kursiv 700 700 kursiv 800 800 kursiv 900 900 kursiv Lägg till fil Lägg till bild Lägg till ny %s Adminmeny Auto Bakgrundsfixering Bakgrundsfärg Bakgrundsbild Bakgrundens position Repetition av bakgrund Bakgrundsstorlek Fet 700 Fet 700 kursiv Bokstil 300 Bokstil 300 kursiv Botten Versalisering Centrerat Centrerat botten Centrerat mitten Centrerat toppen Ändra fil Ändra bild Ändra bild Välj bild Konfigurationen är inte definierad för fältet %1$s – Se %2$s för detaljer om hur man lägger till fält på rätt sätt. Inneslut Fyll Anpassar Anpassar &#9656; %s David Vongries Standard Standard Visa inte detta igen Extra fet 800 Extra fet 800 kursiv Fixerad Textfärg Typsnittsfamilj Textstorlek Typsnittsstil Typsnittsvariant Typsnittsvikt Google Fonts Höjd Bildredigering Viktigt uppgraderingsmeddelande: Initial Installera tillägg Ogiltigt värde Kursiv Justera Kirki Customizer Framework Kirki-fält kan inte läggas till via customize_register. Lägg till dem direkt eller vid init. Vänster Vänster botten Vänster centrerat Vänster toppen Teckenmellanrum Lätt 200 Lätt 200 kursiv Begränsa: %s rader Radhöjd Linje genom Gemener Marginal botten Marginal toppen Media Medium 500 Medium 500 kursiv Ingen fil vald Ingen bild vald Ingen repetering Ingen bild vald Inga Normal 400 Normal 400 kursiv Aviseringar Av På Överstreck Inläggsformat Inlägg Produkter Normal Ta bort Repetera alla Repetera horisontellt Repetera vertikalt Höger Höger botten Höger centrerat Höger toppen Rulla Välj en sida Välj bild Välj … Semifet 600 Semifet 600 kursiv Storlek Socialt Solid Sortering Mellanrum Standardtypsnitt Taxonomier Textplacering Textdekoration Textomvandling Toppen Skrivfel hittades i fält %s – setting istället för settings. Ultrafet 900 Ultrafet 900 kursiv Ultralätt 100 Ultralätt 100 kursiv Understreck Versaler Vågig Välkomstskärm Widgetar Bredd Ordmellanrum Ditt tema använder ett Font Awesome-fält för ikoner. För att undvika problem med ikoner som inte visas på din front-end rekommenderar vi att du installerar det officiella Font-Awesome tillägget. https://davidvongries.com/ https://kirki.org rad 