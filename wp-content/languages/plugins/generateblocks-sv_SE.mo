��    &      L  5   |      P  W   Q     �     �     �     �     �     �  	   �       1      ,   R          �  	   �     �  	   �  )   �     �     �     �                 
   2  
   =  �   H     �  -   �          !     1     =  	   I     S     [     a     |  *  �  R   �       	   $     .     6     >     W  
   o     z  .   �  ,   �     �     �  
   	     	     	  0   )	     Z	     i	     q	     �	     �	     �	     �	     �	  �   �	     ^
  0   x
     �
     �
     �
     �
  	   �
     �
     �
                                                                                         $   
   !         	       #   &              %      "                                                      A small collection of lightweight WordPress blocks that can accomplish nearly anything. Active websites Angle %s Angles Buttons Buttons Documentation CSS files regenerated. Container Container Documentation Craft text-rich content with advanced typography. Create advanced layouts with flexible grids. Curve %s Curves Dashboard Documentation Downloads Drive conversions with beautiful buttons. GenerateBlocks Grid Grid Documentation Happy customers Headline Headline Documentation Learn More Learn more Looking for a WordPress theme? GenerateBlocks and GeneratePress are built with the same principles in mind and complement each other perfectly. No changes found. Organize your content into rows and sections. Settings Settings saved. Tom Usborne Triangle %s Triangles Wave %s Waves https://generateblocks.com https://tomusborne.com PO-Revision-Date: 2021-10-26 20:50:24+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: sv_SE
Project-Id-Version: Plugins - GenerateBlocks - Stable (latest release)
 En liten samling lättviktiga WordPress-block som kan åstadkomma nästan allting. Aktiva webbplatser Vinkel %s Vinklar Knappar Dokumentation om knappar CSS-filer återskapade. Behållare Dokumentation om behållare Skapa formaterad text med avancerad typografi. Skapa avancerad layout med flexibla rutnät. Kurva %s Kurvor Adminpanel Dokumentation Nedladdningar Få högre konverteringsgrad med vackra knappar. GenerateBlocks Rutnät Dokumentation om rutnät Nöjda kunder Rubrik Dokumentation om rubriker Läs mer Läs mer Letar du efter ett WordPress-tema? GenerateBlocks och GeneratePress är byggda enligt samma principer i åtanke och kompletterar varandra perfekt. Inga ändringar hittades. Organisera ditt innehåll i rader och sektioner. Inställningar Inställningar sparade. Tom Usborne Triangel %s Trianglar Våg %s Vågor https://generateblocks.com https://tomusborne.com 